#!/usr/bin/env python

import cairo
import getopt
from gi.repository import Gtk
from gi.repository import GLib
import os
import sys

usage = """
usage: stencil.py [OPTIONS]

This utility will create a stencil for a given GtkWidget using the
parameters provided. Additionally, it can stencil an entire GtkBuilder
formatted XML file.

To stencil an entire GtkBuilder XML file, use the following to stencil
every widget in "filename.xml" into the directory named "generated".
Each stencil will be named using the widget name such as "widget1.png".

    stencil.py -u filename.xml -o generated

To stencil a single widget, you can specify the type and properties
you would like.

    stencil.py -t GtkEntry "text=My Text"

"""

def stencilWidget(widget, filename):
    surface = cairo.ImageSurface(cairo.FORMAT_RGB24,
                                 widget.get_allocation().width,
                                 widget.get_allocation().height)
    context = cairo.Context(surface)
    widget.draw(context)
    surface.write_to_png(filename)
    del context
    del surface

def getAllIds(elem):
    ids = []
    if elem.tag == 'object':
        ids.append(elem.get('id'))
    for child in elem.getchildren():
        ids.extend(getAllIds(child))
    return ids

def fixup(obj):
    if isinstance(obj, Gtk.Toolbar):
        # try to apply styling missing in glade
        obj.get_style_context().add_class(Gtk.STYLE_CLASS_PRIMARY_TOOLBAR)
        if not isinstance(obj.get_parent(), Gtk.Box) or \
           not isinstance(obj.get_parent().get_parent(), Gtk.Window):
            obj.get_style_context().remove_class(Gtk.STYLE_CLASS_PRIMARY_TOOLBAR)
            obj.get_style_context().add_class(Gtk.STYLE_CLASS_INLINE_TOOLBAR)

def stencilUiFile(filename, outdir):
    if not os.path.isdir(outdir):
        os.makedirs(outdir)
    b = Gtk.Builder()
    b.add_from_file(filename)

    # Start showing all the toplevels.
    from lxml import etree
    root = etree.ElementTree()
    root.parse(filename)
    names = []
    for element in root.findall('object'):
        names.extend(getAllIds(element))
    for name in names:
        obj = b.get_object(name)
        fixup(obj)
        if isinstance(obj, Gtk.Widget) and obj.is_toplevel():
            obj.show()

    # Snapshot the widgets.
    def callback(*args):
        try:
            for name in names:
                obj = b.get_object(name)
                if isinstance(obj, Gtk.Widget):
                    path = os.path.join(outdir, name + '.png')
                    stencilWidget(obj, path)
        except Exception, e:
            print >> sys.stderr, repr(e)
        Gtk.main_quit()

    GLib.timeout_add(2000, callback, None)
    Gtk.main()

def stencil(args):
    """
    This function will create a stencil widget using the parameters provided
    in args. If a GtkBuilder file is specified, it will every GtkWidget found
    in the UI description into the specified folder.
    """
    shortOpts, longOpts = ['hu:o:', ['help', 'ui=', 'outdir=']]
    try:
        opts, args = getopt.getopt(args, shortOpts, longOpts)
    except getopt.GetoptError, e:
        print >> sys.stderr, repr(e)
        return -1

    uiFile = None
    outdir = 'generated'

    for o,a in opts:
        if o in ('-h', '--help'):
            print >> sys.stdout, usage
            return 0
        elif o in ('-u', '--ui'):
            uiFile = a
        elif o in ('-o', '--outdir'):
            outdir = a

    try:
        if uiFile and outdir:
            stencilUiFile(uiFile, outdir)
            return 0
        else:
            print 'TODO: Implement command line builder.'
            print >> sys.stderr, usage
            return -1
    except Exception, e:
        print >> sys.stderr, repr(e)
        Gtk.main_quit()
        return -1

if __name__ == '__main__':
    sys.exit(stencil(sys.argv[1:]))
